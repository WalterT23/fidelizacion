import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.css']
})
export class ClienteComponent implements OnInit {

  formulario!: FormGroup;
  listado: any = [];

  constructor() { }

  ngOnInit() {
    this.initForm();
    this.baseClientes();
  }

  initForm() {
    this.formulario = new FormGroup({
      nombre: new FormControl(null),
      apellido: new FormControl(null),
      documento: new FormControl(null),
      tipo: new FormControl(null),
      nacionalidad: new FormControl(null),
      email: new FormControl(null),
      telefono: new FormControl(null),
      fecha: new FormControl(null),
    })
  }

  get getNombre(): any {
    return this.formulario.get('nombre');
  }

  get getApellido(): any {
    return this.formulario.get('apellido');
  }

  get getDocumento(): any {
    return this.formulario.get('documento');
  }

  get getTipo(): any {
    return this.formulario.get('tipo');
  }

  get getNacionalidad(): any {
    return this.formulario.get('nacionalidad');
  }

  get getEmail(): any {
    return this.formulario.get('email');
  }

  get getTelefono(): any {
    return this.formulario.get('telefono');
  }

  get getFecha(): any {
    return this.formulario.get('fecha');
  }

  get validar(): boolean {
    if (this.listado && this.listado.length > 0) return false;
    return true;
  }

  get comprobarDatos(): boolean {
    if (!this.getNombre.value) return false;
    if (!this.getApellido.value) return false;
    if (!this.getDocumento.value) return false;
    if (!this.getTipo.value) return false;
    if (!this.getNacionalidad.value) return false;
    if (!this.getEmail.value) return false;
    if (!this.getTelefono.value) return false;
    if (!this.getFecha.value) return false;
    return true;
  }

  agregarCliente() {
    if (this.comprobarDatos && this.listado) {
      let valor = this.listado.filter((item:any) => item.documento == this.getDocumento.value);
      if (valor && valor.length>0) {
        valor[0].nombre = this.getNombre.value;
        valor[0].apellido = this.getApellido.value;
        valor[0].documento = this.getDocumento.value;
        valor[0].tipo = this.getTipo.value;
        valor[0].nacionalidad = this.getNacionalidad.value;
        valor[0].email = this.getEmail.value;
        valor[0].telefono = this.getTelefono.value;
        valor[0].fecha = this.getFecha.value;
      } else {
        let obj:any = {};
        obj.nombre = this.getNombre.value;
        obj.apellido = this.getApellido.value;
        obj.documento = this.getDocumento.value;
        obj.tipo = this.getTipo.value;
        obj.nacionalidad = this.getNacionalidad.value;
        obj.email = this.getEmail.value;
        obj.telefono = this.getTelefono.value;
        obj.fecha = this.getFecha.value;
        this.listado.push(obj);
      }
      this.formulario.reset();
    }
  }

  editarRegistro(valor:any) {
    if (this.listado && this.listado.length>0 && valor) {
      let valorx = this.listado.filter((item:any) => item.documento == valor.documento);
      if (valorx && valorx.length>0) {
        let valorBuscado = valorx[0];
        this.getNombre.setValue(valorBuscado.nombre);
        this.getApellido.setValue(valorBuscado.apellido);
        this.getDocumento.setValue(valorBuscado.documento);
        this.getTipo.setValue(valorBuscado.tipo);
        this.getNacionalidad.setValue(valorBuscado.nacionalidad);
        this.getEmail.setValue(valorBuscado.email);
        this.getTelefono.setValue(valorBuscado.telefono);
        this.getFecha.setValue(valorBuscado.fecha);
      }
    }
  }

  eliminarRegistro(valor:any) {
    if (this.listado && this.listado.length>0 && valor) {
       this.listado =  this.listado.filter((item:any) => item.documento !== valor.documento);
    }
  }

  limpiarVista() {
    this.formulario.reset();
  }

  baseClientes() {
    this.listado = [
      {nombre:"Walter", apellido: "Torales", documento: "4568544", tipo: "CI", nacionalidad: "Paraguaya", email: "wtoralesv@gmail.com", telefono: "0984968001", fecha:"1992-05-23"},
      {nombre:"Diego", apellido: "Quiñonez", documento: "4659789", tipo: "CI", nacionalidad: "Paraguaya", email: "diego@gmail.com", telefono: "0984954781", fecha:"	1994-05-23"}
    ]
  }

}
